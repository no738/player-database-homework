﻿using System;

namespace PlayerPosition
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.CursorVisible = false;

            var defaultNickname = "nickname";
            var defaultLevel = 1;
            var defaultPosition = new Vector2(2, 2);

            var player = new Player(defaultNickname, defaultLevel, defaultPosition);

            var dataBase = new DataBase();
            dataBase.AddPlayer(player);
        }
    }
}