﻿using System;
using System.Collections.Generic;

namespace PlayerPosition
{
    public class DataBase
    {
        private readonly HashSet<Player> _players = new ();
        public IEnumerable<IReadOnlyPlayer> Players => _players;

        public void AddPlayer(Player player)
        {
            if (_players.Contains(player))
            {
                throw new InvalidOperationException("Player already presented in DataBase!");
            }

            _players.Add(player);
        }

        public void RemovePlayer(Player player)
        {
            if (_players.Contains(player))
            {
                throw new InvalidOperationException("Player not presented in DataBase!");
            }

            _players.Remove(player);
        }

        public bool TryChangePlayerBanStatus(int playerId, bool banValue)
        {
            foreach (Player player in _players)
            {
                if (player.Id != playerId)
                {
                    continue;
                }

                player.IsBanned = banValue;
                return true;
            }

            return false;
        }
    }
}
