﻿using System;

namespace PlayerPosition
{
    public class Player : IReadOnlyPlayer
    {
        private static int _nextId;
        private readonly Vector2 _position;

        public Player(string name, int level, Vector2 defaultPosition)
        {
            Id = _nextId++;
            Name = name;
            Level = level;
            Position = defaultPosition;
        }

        public int Id { get; }
        public string Name { get; }
        public int Level { get; }
        public bool IsBanned { get; set; } = false;

        public Vector2 Position
        {
            get => _position;
            private init
            {
                if (value.X < 0 || value.Y < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }
                
                _position = value;
            }
        }
    }

    public interface IReadOnlyPlayer
    {
        public string Name { get; }
        public int Level { get; }
        public bool IsBanned { get; }
        public Vector2 Position { get; }
    }
}